Webpack es6 bundler.

Requirements node.js latest, npm i -g webpack webpack-dev-server, npm i.

Dev-server:
  npm run dev

Production build:
  npm run production                   - for unix users
  set NODE_ENV=production && webpack   - for windows users